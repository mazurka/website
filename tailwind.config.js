module.exports = {
  purge: ["./src/**/*.js", "./src/**/*.jsx", "./src/**/*.ts", "./src/**/*.tsx"],
  theme: {
    fontFamily: {
      'sans': ["Raleway", "Roboto", "Ubuntu", "Helvetica Neue", "sans-serif"],
      'serif': [
        "Lora",
        "Georgia",
        "Cambria",
        "DejaVu Serif",
        "Bitstream Vera Serif",
        "Liberation Serif",
        "serif",
      ],
      'mono': [
        "Menlo",
        "Monaco",
        "Consolas",
        "Liberation Mono",
        "Courier New",
        "monospace",
      ],
      'logo': ["Shadows\\ Into\\ Light"]
    },
    extend: {
      colors: {
        'green': {
          100: '#F3F7F4',
          200: '#E1ECE3',
          300: '#CFE1D1',
          400: '#ABCAAF',
          500: '#87B38D',
          600: '#7AA17F',
          700: '#516B55',
          800: '#3D513F',
          900: '#29362A',
          },
        mazurka: {
          'white': '#fdfffc',
          'yellow': '#edeec9',
          'green': '#87b38d',
          'pink': '#ff9fb2',
          'purple': '#631d76',
          'purple-lighter': '#7c2494'
        }
      },
      minWidth: {
        '0': '0',
        '1/4': '25%',
        '1/2': '50%',
        '3/4': '75%',
        'full': '100%',
      },
    },
  },
  variants: {},
  plugins: [],
};
