// custom typefaces
import "typeface-lora"
import "typeface-raleway"
import "./src/css/style.css"

// export const onClientEntry = () => {
//   const userLang = navigator.language.substr(0, 2)
//   const supportedLocales = ["fr"]

//   if (
//     userLang !== "en" &&
//     !window.location.pathname.startsWith(`/${userLang}`) &&
//     supportedLocales.includes(userLang) &&
//     !window.location.pathname.startsWith(`/blog`)
//   ) {
//     window.location.pathname = `/${userLang}${window.location.pathname}`
//   }
// }

// import "prismjs/themes/prism.css"
