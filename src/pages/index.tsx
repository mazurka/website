// Gatsby supports TypeScript natively!
import React from "react"
import { PageProps, graphql } from "gatsby"
import { useIntl, defineMessages } from "gatsby-plugin-intl"


import Layout from "../components/layout"
import SEO from "../components/seo"
import Landing from "../components/landing"

type Data = {
  site: {
    siteMetadata: {
      title: string
    }
  }
}

const messages = defineMessages({
  seo_title: {
    id: "index.seo.title",
    defaultMessage: "Mazurka, la boite de réception à plusieurs mains"
  }
})

const BlogIndex = ({ data, location }: PageProps<Data>) => {
  const siteTitle = data.site.siteMetadata.title
  const intl = useIntl()

  return (
    <Layout location={location} title={siteTitle}>
        <SEO title={intl.formatMessage(messages.seo_title)} />
        <Landing />
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    
  }
`
