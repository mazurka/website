// Gatsby supports TypeScript natively!
import React from "react"
import Image from "gatsby-image"
import { useStaticQuery, graphql } from "gatsby"
import { useIntl, injectIntl, defineMessages, FormattedMessage } from "gatsby-plugin-intl"

import FeaturesPoint from "./featuresPoint"
import PricingPoint from "./pricingPoint"
import Values from "./values"
import NewsletterForm from './newsletterForm'

const messages = defineMessages({
  survey_url:
    {
      id: 'survey.url',
      defaultMessage:'https://contact347065.typeform.com/to/TGIfW2'
    }
})

const Landing = () => {
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
        }
      }
      heroImage: file(relativePath: { eq: "mazu-255.png" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
      typewriter: file(relativePath: { eq: "typewriter.png" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
      stamp: file(relativePath: { eq: "stamp.png" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
      letter: file(relativePath: { eq: "letter.png" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
      star: file(relativePath: { eq: "star.svg" }) {
        publicURL
      }
    }
  `)

  const intl = useIntl()

  const typeformRef = React.useRef(null)
  React.useEffect(() => {
    embedTypeform()
  }, [])

  let embedTypeform = function () {
    if (typeof window !== `undefined`) {
      const typeformEmbed = require("@typeform/embed")
      const embedElement = typeformRef.current
      console.log(embedElement)
      console.log(typeformEmbed)
      typeformEmbed.makeWidget(
        embedElement,
        intl.formatMessage(messages.survey_url),
        {
          buttonText: "Répondre à quelques questions",
          onSubmit: () => {
            if (typeof window !== "undefined" && window._paq) {
              window._paq.push(['trackEvent', 'Form', 'Submitted']);
            }
          }
        }
      )
    }
  }

  return (
    <>
      {/* HERO - ACCUEIL */}
      <section
        className="bg-mazurka-white md:h-auto md:mb-20 section-padding flex flex-col-reverse md:flex-row md:items-center"
        id="accueil"
      >
        <div className="flex flex-col justify-around md:w-1/2 mt-8 md:mx-6">
          <div>
            <h1 className="text-5xl md:text-4xl xl:text-6xl text-center md:text-left font-serif font-semibold leading-tight">
              <FormattedMessage id="landing.hero.headline"
               defaultMessage="La boite de réception à plusieurs mains" />
            </h1>
            <p className="text-xl md:text-lg xl:text-2xl text-center md:text-left mt-4 mb-8 lg:my-10">
              <FormattedMessage id="landing.hero.tagline" 
              defaultMessage="Trier, discuter et répondre en équipe à tous vos contacts." />
            </p>
          </div>

          <NewsletterForm pColor="text-gray-600" inputContainer="newsletter-hero-input-container" inputStyle="input-cta placeholder-gray-600" buttonStyle="newsletter-hero-button" />

        </div>
        <div className="mx-auto md:block w-full md:w-1/2 md:mx-6">
          <Image
            fluid={data.heroImage.childImageSharp.fluid}
            className="transparent-img-shadow"
            alt="dance with mazurka"
          />
        </div>
      </section>
      <svg
        className="top-divider"
        xmlns="http://www.w3.org/2000/svg"
        width="100%"
        height="81"
        fill="none"
        viewBox="0 0 3000 81"
      >
        <g>
          <path
            fill="#fdfffc"
            d="M0 0v43c19.4.86 38.83 1.45 58.22 1 29.51-.64 59-2.67 88.49-4.2 18.87-1 37.77-1.69 56.58-3.36 38-3.37 75.85-7.57 113.81-10.93 25-2.23 50.15-3.78 75.25-5.3 42.4-2.57 84.79-5.33 127.23-7.14 38.24-1.62 76.53-2.39 114.8-2.89 18.91-.25 37.87.5 56.76 1.62 31.42 1.87 62.8 4.32 94.19 6.67 22 1.64 44 3.18 65.89 5.37 40.73 4.07 81.39 9 122.15 12.8 38.81 3.62 77.7 6.49 116.58 9.3 25.92 1.88 51.88 3 77.83 4.58 25 1.48 50 3.18 75 4.48 34.61 1.8 69.24 3.11 103.84 5.06 21.16 1.2 42.29 3.07 63.42 4.89 15.69 1.35 31.34 3.2 47 4.66 23.48 2.19 47 4.55 70.48 6.24 24.64 1.76 49.36 2.49 74 4.3 12.45.92 24.82.06 35.58-5.49 7.38-3.81 13.9-6.3 22.27-6 11.38.37 22.83-.7 34.23-1.4 15.65-1 31.28-2.64 46.94-3.15 18.84-.6 37.71-.25 56.57-.43 3.16 0 7 1.22 9.49-3.51l-7.49-3c11.23-4.31 17.49-4.57 28.59-1.89 7.26 1.75 14.51 3 22 .9a73.612 73.612 0 0115-2.63c16.11-1 32.24-1.45 48.35-2.31 9.72-.51 19.41-1.71 29.13-1.88 3.23-.06 8-3.59 10.08 2.49.13.4 2.24.74 2.83.3 9-6.76 17.93-1.65 26.45.79 7.43 2.14 14.48 3.16 22 1.49 2.55-.57 7.11-1.2 7.67-.15 3.33 6.17 8.77 3.94 13.43 4 8.1.14 17.18 2 24.06-1 9.44-4.11 18.59-5.47 28.52-5.95 18.88-.9 37.72-2.66 56.59-3.82 5.8-.35 11.66.47 17.46.12 24.32-1.44 48.62-3.36 72.95-4.62 11.77-.61 23.6-.07 35.4-.28 2.58-.05 5.64-.42 7.61-1.84 6.21-4.5 12.62-4.95 19.74-2.93 7.12 2.02 14.37 4.57 21.7 5.13 15.62 1.18 31 5.24 47 3.34 8.82-1 17.87-.3 26.81-.16 16.76.27 33.52 1.28 50.26.9 23.62-.55 47.13-.09 70.58 2.9 8.93 1.14 18.06 1 26.91 2.49 34.94 6.02 70.33 9.03 105.79 9 3.93 0 8-.11 11.77.7 14.11 3 28.08 1.29 42.13-.36a52.998 52.998 0 0115.25.26 67.31 67.31 0 0030.33-1.71 48.754 48.754 0 0113.84-2.24c10.21.142 20.41.827 30.55 2.05 10.32 1.32 21.1-4.39 31.09 2.17 1.09.71 3.17.33 4.66-.06 15.36-4 30.75-4.32 46.49-2.1 8 1.13 16.37-.12 24.56 0 13.71.27 27.4-1.263 40.71-4.56 12.69-3.12 26.39-2.13 39.64-3.07 3.12-.23 6.19-1 9.31-1.43 7.87-1.04 15.74-2.04 23.63-3V0H0z"
          ></path>
        </g>
      </svg>

      {/* MISSION */}

      <section className="section-padding bg-green-500 text-white" id="mission">
        <div className="lg:max-w-4xl lg:mx-auto">
          <h2 className="my-h2 text-center p-8 text-green-100 whitespace-pre-line">
            <FormattedMessage
              id="landing.mission.title"
              defaultMessage={`L'email n'a pas été conçu\npour les collectifs`}
            />
          </h2>
          <FormattedMessage
            id="landing.mission.text"
            defaultMessage={`
              <p>
                L'email a été conçu pour des personnes seules\u{202F}:
                <strong>une boite mail = une personne.</strong>
                Sauf que bien souvent un mail ce n'est pas une personne\u{202F}:
                c'est une association, c'est une équipe, c'est une entreprise,
                c'est un collectif ou encore une famille.
              </p>
              <quote>
                Qui va répondre à cet email\u{202F}?
                Ah, mais Camille lui a déjà répondu par Facebook\u{202F}?
                Attends, elle est où la dernière version du brouillon déjà\u{202F}?
                Ça serait bien que Fred relise…
                Je vais passer un coup de fil pour relancer.
                Oh non, on devait répondreavant le 23\u{202F}!
              </quote>
              <p>
                <strong>S'occuper d'une seule boîte mail était compliqué…
                et d'autres canaux se sont rajoutés\u{202F}:</strong>
                Facebook, une deuxième adresse mail, des SMS, les commentaires du
                blog, Twitter, Signal, WhatsApp…
              </p>
              <p>
                <strong>Depuis des années, nous avons tout testé\u{202F}:</strong>
                webmails partagés, alias, listes de diffusion et même des logiciels
                transformant chaque échange en «\u{202F}ticket\u{202F}» Tordre ces
                outils pour qu'ils fonctionnent à plusieurs relève de la
                prouesse\u{202F}: une chorégraphie millimétrée, trop organisée,
                trop technique, réservée aux super-geeks.
              </p>
              <p>
                <strong>Assez bricolé.</strong> Nous nous sommes retrouvé·es\u{202F}:
                designers, militant·es, développeur·euses, <strong>le moment est venu
                de construire la solution dont nous avons besoin.</strong>
              </p>
            `}
            values={{
              p: (...chunks) => <p className="mission-p">{chunks}</p>,
              strong: (...chunks) => <strong className="font-bold">{chunks}</strong>,
              quote: (...chunks) => (
                <p className="mission-p my-quote">{chunks}</p>
              ),
            }}
          />
        </div>
      </section>
      <svg
        className="divider-bottom"
        xmlns="http://www.w3.org/2000/svg"
        width="100%"
        height="81"
        fill="none"
        viewBox="0 0 3000 81"
      >
        <g>
          <path
            fill="#fdfffc"
            d="M0 0v43c19.4.86 38.83 1.45 58.22 1 29.51-.64 59-2.67 88.49-4.2 18.87-1 37.77-1.69 56.58-3.36 38-3.37 75.85-7.57 113.81-10.93 25-2.23 50.15-3.78 75.25-5.3 42.4-2.57 84.79-5.33 127.23-7.14 38.24-1.62 76.53-2.39 114.8-2.89 18.91-.25 37.87.5 56.76 1.62 31.42 1.87 62.8 4.32 94.19 6.67 22 1.64 44 3.18 65.89 5.37 40.73 4.07 81.39 9 122.15 12.8 38.81 3.62 77.7 6.49 116.58 9.3 25.92 1.88 51.88 3 77.83 4.58 25 1.48 50 3.18 75 4.48 34.61 1.8 69.24 3.11 103.84 5.06 21.16 1.2 42.29 3.07 63.42 4.89 15.69 1.35 31.34 3.2 47 4.66 23.48 2.19 47 4.55 70.48 6.24 24.64 1.76 49.36 2.49 74 4.3 12.45.92 24.82.06 35.58-5.49 7.38-3.81 13.9-6.3 22.27-6 11.38.37 22.83-.7 34.23-1.4 15.65-1 31.28-2.64 46.94-3.15 18.84-.6 37.71-.25 56.57-.43 3.16 0 7 1.22 9.49-3.51l-7.49-3c11.23-4.31 17.49-4.57 28.59-1.89 7.26 1.75 14.51 3 22 .9a73.612 73.612 0 0115-2.63c16.11-1 32.24-1.45 48.35-2.31 9.72-.51 19.41-1.71 29.13-1.88 3.23-.06 8-3.59 10.08 2.49.13.4 2.24.74 2.83.3 9-6.76 17.93-1.65 26.45.79 7.43 2.14 14.48 3.16 22 1.49 2.55-.57 7.11-1.2 7.67-.15 3.33 6.17 8.77 3.94 13.43 4 8.1.14 17.18 2 24.06-1 9.44-4.11 18.59-5.47 28.52-5.95 18.88-.9 37.72-2.66 56.59-3.82 5.8-.35 11.66.47 17.46.12 24.32-1.44 48.62-3.36 72.95-4.62 11.77-.61 23.6-.07 35.4-.28 2.58-.05 5.64-.42 7.61-1.84 6.21-4.5 12.62-4.95 19.74-2.93 7.12 2.02 14.37 4.57 21.7 5.13 15.62 1.18 31 5.24 47 3.34 8.82-1 17.87-.3 26.81-.16 16.76.27 33.52 1.28 50.26.9 23.62-.55 47.13-.09 70.58 2.9 8.93 1.14 18.06 1 26.91 2.49 34.94 6.02 70.33 9.03 105.79 9 3.93 0 8-.11 11.77.7 14.11 3 28.08 1.29 42.13-.36a52.998 52.998 0 0115.25.26 67.31 67.31 0 0030.33-1.71 48.754 48.754 0 0113.84-2.24c10.21.142 20.41.827 30.55 2.05 10.32 1.32 21.1-4.39 31.09 2.17 1.09.71 3.17.33 4.66-.06 15.36-4 30.75-4.32 46.49-2.1 8 1.13 16.37-.12 24.56 0 13.71.27 27.4-1.263 40.71-4.56 12.69-3.12 26.39-2.13 39.64-3.07 3.12-.23 6.19-1 9.31-1.43 7.87-1.04 15.74-2.04 23.63-3V0H0z"
          ></path>
        </g>
      </svg>

      {/* VALEURS */}
      <section className="section-padding bg-mazurka-white" id="valeurs">
        <div className="lg:max-w-6xl lg:mx-auto">
          <div className="md:m-8">
            <h2 className="big-h2 mb-8 sm:mb-24 text-center whitespace-pre-line">
              <FormattedMessage
                id="values.title"
                defaultMessage={`C'est à nos outils de nous ressembler\net non l'inverse.`}
              />
            </h2>
          </div>

          <div className="sm:w-4/5 mx-auto">
            <Values />
          </div>
        </div>
      </section>
      <svg
        className="top-divider"
        xmlns="http://www.w3.org/2000/svg"
        width="100%"
        height="81"
        fill="none"
        viewBox="0 0 3000 81"
      >
        <g>
          <path
            fill="#fdfffc"
            d="M0 0v43c19.4.86 38.83 1.45 58.22 1 29.51-.64 59-2.67 88.49-4.2 18.87-1 37.77-1.69 56.58-3.36 38-3.37 75.85-7.57 113.81-10.93 25-2.23 50.15-3.78 75.25-5.3 42.4-2.57 84.79-5.33 127.23-7.14 38.24-1.62 76.53-2.39 114.8-2.89 18.91-.25 37.87.5 56.76 1.62 31.42 1.87 62.8 4.32 94.19 6.67 22 1.64 44 3.18 65.89 5.37 40.73 4.07 81.39 9 122.15 12.8 38.81 3.62 77.7 6.49 116.58 9.3 25.92 1.88 51.88 3 77.83 4.58 25 1.48 50 3.18 75 4.48 34.61 1.8 69.24 3.11 103.84 5.06 21.16 1.2 42.29 3.07 63.42 4.89 15.69 1.35 31.34 3.2 47 4.66 23.48 2.19 47 4.55 70.48 6.24 24.64 1.76 49.36 2.49 74 4.3 12.45.92 24.82.06 35.58-5.49 7.38-3.81 13.9-6.3 22.27-6 11.38.37 22.83-.7 34.23-1.4 15.65-1 31.28-2.64 46.94-3.15 18.84-.6 37.71-.25 56.57-.43 3.16 0 7 1.22 9.49-3.51l-7.49-3c11.23-4.31 17.49-4.57 28.59-1.89 7.26 1.75 14.51 3 22 .9a73.612 73.612 0 0115-2.63c16.11-1 32.24-1.45 48.35-2.31 9.72-.51 19.41-1.71 29.13-1.88 3.23-.06 8-3.59 10.08 2.49.13.4 2.24.74 2.83.3 9-6.76 17.93-1.65 26.45.79 7.43 2.14 14.48 3.16 22 1.49 2.55-.57 7.11-1.2 7.67-.15 3.33 6.17 8.77 3.94 13.43 4 8.1.14 17.18 2 24.06-1 9.44-4.11 18.59-5.47 28.52-5.95 18.88-.9 37.72-2.66 56.59-3.82 5.8-.35 11.66.47 17.46.12 24.32-1.44 48.62-3.36 72.95-4.62 11.77-.61 23.6-.07 35.4-.28 2.58-.05 5.64-.42 7.61-1.84 6.21-4.5 12.62-4.95 19.74-2.93 7.12 2.02 14.37 4.57 21.7 5.13 15.62 1.18 31 5.24 47 3.34 8.82-1 17.87-.3 26.81-.16 16.76.27 33.52 1.28 50.26.9 23.62-.55 47.13-.09 70.58 2.9 8.93 1.14 18.06 1 26.91 2.49 34.94 6.02 70.33 9.03 105.79 9 3.93 0 8-.11 11.77.7 14.11 3 28.08 1.29 42.13-.36a52.998 52.998 0 0115.25.26 67.31 67.31 0 0030.33-1.71 48.754 48.754 0 0113.84-2.24c10.21.142 20.41.827 30.55 2.05 10.32 1.32 21.1-4.39 31.09 2.17 1.09.71 3.17.33 4.66-.06 15.36-4 30.75-4.32 46.49-2.1 8 1.13 16.37-.12 24.56 0 13.71.27 27.4-1.263 40.71-4.56 12.69-3.12 26.39-2.13 39.64-3.07 3.12-.23 6.19-1 9.31-1.43 7.87-1.04 15.74-2.04 23.63-3V0H0z"
          ></path>
        </g>
      </svg>

      {/* FEATURES */}
      <section className="bg-green-100 section-padding" id="features">
        <div className="lg:max-w-6xl lg:mx-auto">
          <h2 className="big-h2 mb-8 text-center">
            <FormattedMessage
            id="features.title" defaultMessage="Ce que fera Mazurka" />
          </h2>

          <div className="sm:w-4/5 mx-auto">
            <div className="features-container">
              <div className="sm:w-1/2 mt-10 sm:mt-0">
                <h3 className="h3-features"><FormattedMessage id="features.collab.title" defaultMessage="Collaborer en direct" /></h3>
                <ul className="mt-8">
                  <FeaturesPoint>
                    <FormattedMessage id="features.collab.1" defaultMessage="S'attribuer une tâche ou notifier une personne" />
                  </FeaturesPoint>

                  <FeaturesPoint>
                    <FormattedMessage id="features.collab.2" defaultMessage="Discuter directement sous le message" />
                  </FeaturesPoint>
                  <FeaturesPoint>
                    <FormattedMessage id="features.collab.3" defaultMessage="Rédiger à plusieurs mains et valider une réponse" />
                  </FeaturesPoint>
                </ul>
                <p className="mt-4">
                  <FormattedMessage id="features.collab.hint"
                  defaultMessage="<strong>Une seule plateforme pour se coordonner</strong> 🤹"
                  values={{
                  strong: (...chunks) => <strong className="p-features text-gray-600 font-bold">{chunks}</strong>,
                  }}
                  />
                </p>
              </div>
              <div className="card-avatar-container">
                <Image
                  className="card-avatar-image transparent-img-shadow"
                  fluid={data.typewriter.childImageSharp.fluid}
                  style={{}}
                  alt="writing"
                />
                {/* <img className="card-avatar-image transparent-img-shadow" src="assets/typewriter.png" alt="writing"/> */}
              </div>
            </div>

            <div className="features-container sm:flex-row-reverse">
              <div className="sm:w-1/2 mt-10 sm:mt-0">
                <h3 className="h3-features"><FormattedMessage id="features.lifecycle.title" defaultMessage="Visualiser la vie d'un message"/></h3>
                <ul className="mt-8">
                  <FeaturesPoint>
                    <FormattedMessage id="features.lifecycle.0" defaultMessage="Fixer les règles de tri et indiquer les échéances" />
                  </FeaturesPoint>
                  <FeaturesPoint>
                    <FormattedMessage id="features.lifecycle.1" defaultMessage="Ajouter des messages à l'ordre du jour d'une réunion" />
                  </FeaturesPoint>
                  <FeaturesPoint>
                    <FormattedMessage id="features.lifecycle.2" defaultMessage="Faire remonter les messages non répondus" />
                  </FeaturesPoint>
                </ul>
                <p className="mt-4">
                  <FormattedMessage id="features.lifecycle.hint"
                  defaultMessage="<strong>Pas de message laissé sur le bord de la route</strong> 😉"
                  values={{
                  strong: (...chunks) => <strong className="p-features text-gray-600 font-bold">{chunks}</strong>,
                  }}
                  />
                </p>
              </div>
              <div className="card-avatar-container">
                <Image
                  className="card-avatar-image transparent-img-shadow"
                  fluid={data.stamp.childImageSharp.fluid}
                  style={{}}
                  alt="stamp"
                />
                {/* <img className="card-avatar-image transparent-img-shadow" src="assets/stamp.png" alt="stamp"/> */}
              </div>
            </div>

            <div className="features-container mb-0">
              <div className="sm:w-1/2 mt-10 sm:mt-0">
                <h3 className="h3-features">
                  <FormattedMessage id="features.regroup.title" defaultMessage="Regrouper toutes les messageries" />
                </h3>
                <ul className="mt-8">
                  <FeaturesPoint>
                    <FormattedMessage id="features.regroup.0" defaultMessage="Différentes boites mails pour différentes équipes" />
                  </FeaturesPoint>
                  <FeaturesPoint>
                    <FormattedMessage id="features.regroup.1" defaultMessage={`Rassembler les messages privés des médias sociaux\u{202F}:
                    Facebook, Twitter, Instagram…`} />
                  </FeaturesPoint>
                  <FeaturesPoint>
                    <FormattedMessage id="features.regroup.2" defaultMessage={`… et des messageries mobiles\u{202F}: Signal, Telegram,
                    WhatsApp, Matrix`} />
                  </FeaturesPoint>
                </ul>
                <p className="mt-4">
                <FormattedMessage id="features.regroup.hint"
                  defaultMessage="<strong>Et de nouveaux canaux au fur à mesure du développement</strong> 🎈"
                  values={{
                  strong: (...chunks) => <strong className="p-features text-gray-600 font-bold">{chunks}</strong>,
                  }}
                  />
                </p>
              </div>
              <div className="card-avatar-container">
                <Image
                  className="card-avatar-image transparent-img-shadow"
                  fluid={data.letter.childImageSharp.fluid}
                  style={{}}
                  alt="letter"
                />
                {/* <img className="card-avatar-image transparent-img-shadow" src="assets/letter.png" alt="lettre" /> */}
              </div>
            </div>
          </div>
        </div>
      </section>
      <svg
        className="divider-bottom "
        xmlns="http://www.w3.org/2000/svg"
        width="100%"
        height="81"
        fill="none"
        viewBox="0 0 3000 81"
      >
        <g>
          <path
            fill="#fdfffc"
            d="M0 0v43c19.4.86 38.83 1.45 58.22 1 29.51-.64 59-2.67 88.49-4.2 18.87-1 37.77-1.69 56.58-3.36 38-3.37 75.85-7.57 113.81-10.93 25-2.23 50.15-3.78 75.25-5.3 42.4-2.57 84.79-5.33 127.23-7.14 38.24-1.62 76.53-2.39 114.8-2.89 18.91-.25 37.87.5 56.76 1.62 31.42 1.87 62.8 4.32 94.19 6.67 22 1.64 44 3.18 65.89 5.37 40.73 4.07 81.39 9 122.15 12.8 38.81 3.62 77.7 6.49 116.58 9.3 25.92 1.88 51.88 3 77.83 4.58 25 1.48 50 3.18 75 4.48 34.61 1.8 69.24 3.11 103.84 5.06 21.16 1.2 42.29 3.07 63.42 4.89 15.69 1.35 31.34 3.2 47 4.66 23.48 2.19 47 4.55 70.48 6.24 24.64 1.76 49.36 2.49 74 4.3 12.45.92 24.82.06 35.58-5.49 7.38-3.81 13.9-6.3 22.27-6 11.38.37 22.83-.7 34.23-1.4 15.65-1 31.28-2.64 46.94-3.15 18.84-.6 37.71-.25 56.57-.43 3.16 0 7 1.22 9.49-3.51l-7.49-3c11.23-4.31 17.49-4.57 28.59-1.89 7.26 1.75 14.51 3 22 .9a73.612 73.612 0 0115-2.63c16.11-1 32.24-1.45 48.35-2.31 9.72-.51 19.41-1.71 29.13-1.88 3.23-.06 8-3.59 10.08 2.49.13.4 2.24.74 2.83.3 9-6.76 17.93-1.65 26.45.79 7.43 2.14 14.48 3.16 22 1.49 2.55-.57 7.11-1.2 7.67-.15 3.33 6.17 8.77 3.94 13.43 4 8.1.14 17.18 2 24.06-1 9.44-4.11 18.59-5.47 28.52-5.95 18.88-.9 37.72-2.66 56.59-3.82 5.8-.35 11.66.47 17.46.12 24.32-1.44 48.62-3.36 72.95-4.62 11.77-.61 23.6-.07 35.4-.28 2.58-.05 5.64-.42 7.61-1.84 6.21-4.5 12.62-4.95 19.74-2.93 7.12 2.02 14.37 4.57 21.7 5.13 15.62 1.18 31 5.24 47 3.34 8.82-1 17.87-.3 26.81-.16 16.76.27 33.52 1.28 50.26.9 23.62-.55 47.13-.09 70.58 2.9 8.93 1.14 18.06 1 26.91 2.49 34.94 6.02 70.33 9.03 105.79 9 3.93 0 8-.11 11.77.7 14.11 3 28.08 1.29 42.13-.36a52.998 52.998 0 0115.25.26 67.31 67.31 0 0030.33-1.71 48.754 48.754 0 0113.84-2.24c10.21.142 20.41.827 30.55 2.05 10.32 1.32 21.1-4.39 31.09 2.17 1.09.71 3.17.33 4.66-.06 15.36-4 30.75-4.32 46.49-2.1 8 1.13 16.37-.12 24.56 0 13.71.27 27.4-1.263 40.71-4.56 12.69-3.12 26.39-2.13 39.64-3.07 3.12-.23 6.19-1 9.31-1.43 7.87-1.04 15.74-2.04 23.63-3V0H0z"
          ></path>
        </g>
      </svg>

      {/* PRICING */}
      <section className="section-padding bg-mazurka-white" id="prix">
        <div className="lg:max-w-4xl lg:mx-auto">
          <h2 className="big-h2 mb-8 sm:mb-24 text-center whitespace-pre-line">
            <FormattedMessage id="pricing.title" defaultMessage={`Mazurka sera pour\ntout le monde`} />
          </h2>
        </div>
        <div className="lg:flex justify-center">
          <div className="card-container">
            <h2 className="price-title upppercase">
              <FormattedMessage id="pricing.asyouwant.title" defaultMessage="Formule prix libre" />
            </h2>

            <p className="p-card price-txt self-center">
              <FormattedMessage id="pricing.asyouwant.price" defaultMessage="De <strong>1</strong> à 99 € par mois"
                                values={{
                                  strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                                  }}
                />
            </p>
            <p className="my-p">
              <FormattedMessage id="pricing.asyouwant.comment"
              defaultMessage="Parce que l'argent ne devrait jamais être un frein, c'est vous qui
              déciderez du prix qui est juste pour votre groupe." />
            </p>
            <div className="price-text-btn">
              <ul className="mt-6">
                <PricingPoint>
                  <FormattedMessage id="pricing.asyouwant.0"
                  defaultMessage="
                  <strong>Des accès pour toute l'équipe</strong>, quelle que
                  soit sa taille" 
                  values={{
                    strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                    }}

                  />
                </PricingPoint>
                <PricingPoint>
                  <FormattedMessage id="pricing.asyouwant.1"
                  defaultMessage="
                  <strong>5 boites emails</strong> sous la forme {domain}"
                  values={{
                    strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                    domain: <code>@mazurka.email</code>,
                    }}
/>
                </PricingPoint>
                <PricingPoint>
                  <FormattedMessage id="pricing.asyouwant.2"
                  defaultMessage="
                  <strong>10 Go</strong> de données"
                  values={{
                    strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                    }}
/>
                </PricingPoint>
                <PricingPoint>
                  <FormattedMessage id="pricing.asyouwant.3"
                  defaultMessage={`<strong>Les canaux essentiels\u{202F}</strong>: emails, médias
                  sociaux, messageries sécurisés…`}
                                                   values={{
                    strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                    }}
/>
                </PricingPoint>
                <PricingPoint>
                  <FormattedMessage id="pricing.asyouwant.4"
                  defaultMessage="Support par la communauté" 
                  values={{
                    strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                    }}
/>
                </PricingPoint>
              </ul>
            </div>
          </div>
          <div className="card-container">
            <h2 className="price-title uppercase">
              <FormattedMessage id="pricing.pro.title" defaultMessage="Formule pro"/>
              </h2>

            <p className="p-card price-txt self-center">
              <FormattedMessage id="pricing.pro.price" defaultMessage="<strong>99</strong> € par mois" 
              values={{
                strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                }}

              />
            </p>
            <p className="my-p">
              <FormattedMessage id="pricing.pro.comment" defaultMessage="Un plan taillé sur mesure pour les groupes avec des besoins
              conséquents." />
            </p>
            <div className="price-text-btn">
              <ul className="mt-6">
                <PricingPoint>
                  <FormattedMessage id="pricing.pro.0" defaultMessage="<strong>Tout ce qu'il y a dans la formule prix libre</strong>" 
                                    values={{
                                      strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                                      }}
                  
                  />
                </PricingPoint>
                <PricingPoint>
                  <FormattedMessage id="pricing.pro.1" defaultMessage="<strong>Autant de boites emails que nécessaire</strong> avec
                  votre nom de domaine ou {domain}" 
                  values={{
                    strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                    domain: <code>@mazurka.email</code>,
                    }}
/>
                </PricingPoint>
                <PricingPoint>
                  <FormattedMessage id="pricing.pro.2" defaultMessage="<strong>La place pour toutes vos données</strong>"
                                    values={{
                                      strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                                      }}
                   />
                </PricingPoint>
                <PricingPoint>
                  <FormattedMessage id="pricing.pro.3" defaultMessage={`<strong>Des canaux supplémentaires</strong>\u{202F}: envoyer et
                  recevoir des SMS…`} 
                  values={{
                    strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                    }}
/>
                </PricingPoint>
                <PricingPoint>
                  <FormattedMessage id="pricing.pro.4" defaultMessage="<strong>Support personnalisé</strong>" 
                                    values={{
                                      strong: (...chunks) => <strong className="price-strong">{chunks}</strong>,
                                      }}
                  />
                </PricingPoint>
              </ul>
            </div>
          </div>
        </div>
        <div className="my-10 lg:mt-0 bg-white shadow-md md:max-w-xl lg:max-w-screen-lg rounded-md px-6 py-10 mx-auto">
          <p className="p-card font-serif font-semibold uppercase text-center lg:text-left">
            <FormattedMessage id="pricing.diy.title" defaultMessage={`Formule «\u{202F}faire soi-même\u{202F}»`} />
          </p>
          <p className="p-card mb-0">
            <FormattedMessage id="pricing.diy.comment" defaultMessage="Mazurka sera un logiciel libre. Vous pourrez l'installer sur votre
            serveur préféré, sous votre contrôle." />
          </p>
        </div>
      </section>

      {/* FORM */}
      <section className="section-padding bg-gray-200" id="questionnaire">
        <div className="md:max-w-4xl md:mx-auto">
          <h2 className="big-h2 mb-24 text-center whitespace-pre-line">
            <FormattedMessage id="survey.title" defaultMessage={`Vous auriez 2 minutes\npour nous aider\u{202F}?`} />
          </h2>

          <div
            ref={typeformRef}
            id="embedded-typeform"
            className="typeform-widget rounded-lg overflow-hidden"
            style={{ width: "100%", height: "75vh" }}
          >
            {/* Le texte qui suit devrait normalement être remplacé par le formulaire lui-même. */}
            <a
              className="block btn hover:shadow-lg p-4 max-w-md mx-auto text-3xl"
              href={intl.formatMessage(messages.survey_url)}
            >
              <FormattedMessage id="survey.button-label" defaultMessage="Répondre à quelques questions" />
            </a>
          </div>
        </div>
      </section>
    </>
  )
}

export default injectIntl(Landing)
