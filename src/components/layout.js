import React, { useEffect, useState } from "react"
import Footer from "./footer"
import { Link } from "gatsby" // On importe pas de gatsby-plugin-intl  car on veut forcer vers la vf
import LanguageSwitcher from "./languageSwitcher"

const Layout = ({ children, location, title }) => {
  const [move, setMove] = useState("")
  const [scrollY, setScrollY] = useState(0)
  // change state on scroll
  useEffect(() => {
    const handleScroll = () => {
      setScrollY(window.scrollY)
      scrollY < window.scrollY ? setMove("move-top") : setMove("move-down")
      window.scrollY === 0 && setMove("")
    }

    document.addEventListener("scroll", handleScroll, { passive: true })
    return () => {
      // clean up the event handler when the component unmounts
      document.removeEventListener("scroll", handleScroll)
    }
  }, [scrollY])

  let locale = "en"
  if (typeof navigator !== "undefined") {
    locale = navigator.language.substring(0, 2)
  }

  return (
    <div className="bg-mazurka-white">
      <div className="">
        <header
          className={`bg-mazurka-white flex px-8 py-4 xl:px-20  items-baseline justify-between fixed top-0 w-full z-50 ${move} `}
        >
          <Link to="/">
            <p className="text-mazurka-purple text-2xl font-extrabold md:text-3xl md:mx-6 ">
              Mazurka
            </p>
          </Link>
          <div className="flex items-center">
            <LanguageSwitcher />

            {/* On force le link vers la version française car pas de version anglaise (existe en route mais est en fr) */}
            {/* <Link to="/fr/blog">
              <p className="text-lg font-medium ml-4">blog</p>
            </Link> */}
          </div>
        </header>

        <main className="mt-10">{children}</main>
      </div>
      <Footer />
    </div>
  )
}

export default Layout
