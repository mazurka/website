import React from 'react'

export default function blogLayout({children}) {
  return (
    <div className="max-w-3xl mx-auto py-8 px-6">
      {children}
    </div>
  )
}
