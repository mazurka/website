import React, { useState } from "react"
import { useIntl, injectIntl, defineMessages, FormattedMessage } from "gatsby-plugin-intl"

const messages = defineMessages({
  bad_method: {
    id: "form.newsletter.bad_method",
    defaultMessage: "There was a problem with your submission, please try again."
  },
  spam_detected: {
    id: "form.newsletter.spam_detected",
    defaultMessage: "Thank you for trying!"
  },
  invalid_input: {
    id: "form.newsletter.invalid_input",
    defaultMessage: "Oups ! 😧 Le formulaire n'est pas valide. Veuillez réessayer."
  },
  already_known: {
    id: "form.newsletter.already_known",
    defaultMessage: "L’email est déjà dans la liste. Merci encore, on espère vous envoyer des nouvelles bientôt."
  },
  subscribed: {
    id: "form.newsletter.subscribed",
    defaultMessage: "Merci ! 🙏  On espère vous envoyer des nouvelles bientôt !"
  },
  notification_failed: {
    id: "form.newsletter.notification_failed",
    defaultMessage: "Oups ! 😭 Un soucis est arrivé… Il faudrait réessayer plus tard."
  },
  unknown_message: {
    id: "form.newsletter.unknown_message",
    defaultMessage: `Oups ! Une réponse inconnue est arrivée\u{202F}: {message}`
  },
  invalid_reply: {
    id: "form.newsletter.invalid_reply",
    defaultMessage: "Oups ! Une réponse incompréhensible est arrivé. Il faudrait réessayer plus tard."
  },
  fetch_failed: {
    id: "form.newsletter.fetch_failed",
    defaultMessage: "Oups ! Soit il est impossible de joindre le serveur, soit il y a un soucis de configuration. Il faudrait réessayer plus tard."
  },
  email_placeholder: {
    id: "form.newsletter.placeholder",
    defaultMessage: "votre email"
  }
})

const NewsletterForm = ({pColor, inputContainer, buttonStyle, inputStyle}) => {
  const intl = useIntl()
  const [email, setEmail] = useState()
  const [status, setStatus] = useState("ready")
  const [errorMessage, setErrorMessage] = useState()
  const [result, setResult] = useState()

  const handleChange = e => {
    setEmail(e.target.value)
  }

  const lookupMessage = (messageKey, defaultMessage) => {
    if (messageKey in messages) {
      return intl.formatMessage(messages[messageKey]);
    } else {
      return intl.formatMessage({
        ...messages.unknown_message,
        values: {
          message: defaultMessage
        }
      })
    }
  }

  const handleSubmit = e => {
    e.preventDefault()
    setErrorMessage()
    setStatus("sending")
    fetch("https://stats.mazurka.app/register", {
      method: "POST",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email: email }),
    }).then(response => {
      response.json().then(json => {
        if (response.status !== 200) {
          setStatus("failed")
          setErrorMessage(lookupMessage(json.id, json.defaultMessage))
        } else {
          setStatus("submitted")
          setResult(lookupMessage(json.id, json.defaultMessage))
          if (typeof window !== "undefined" && window._paq) {
            window._paq.push(['trackGoal', 2])
          }
        }
      }).catch(error => {
        console.log("⚠ Unable to parse JSON")
        console.log(error)
        setStatus("failed")
        setErrorMessage(intl.formatMessage(messages.invalid_reply))
      })
    }).catch(error => {
      console.log("⚠ fetch failed")
      console.log(error)
      setStatus("failed")
      setErrorMessage(intl.formatMessage(messages.fetch_failed))
    })
  }

  return (
    <>
      {status !== "submitted" ? (
        <form
          className="ajax-form newsletter-form"
          onSubmit={handleSubmit}
        >
          <input
            className="hidden"
            type="text"
            name="url"
            value=""
            placeholder="Laisser vide !"
            disabled={status === "sending"}
          />

          <p className={`newsletter-p ${pColor}`}>
            <FormattedMessage id="form.newsletter.title"
              defaultMessage={`On vous tient au courant\u{202F}?`} />
          </p>
          <div className={inputContainer}>
            <input
              disabled={status === "sending"}
              className={inputStyle}
              value={email}
              type="email"
              name="email"
              onChange={handleChange}
              placeholder={intl.formatMessage(messages.email_placeholder)}
              required
            />
            <button
              type="submit"
              className={`${buttonStyle} ${
                status === "sending" && "sending"
                } `}
            >
              <FormattedMessage id="form.newsletter.button"
                defaultMessage="Recevoir des nouvelles" />
            </button>
            {status === "failed" && (
              <span className="error block text-xl md:text-lg xl:text-2xl text-center md:text-left mt-4 text-red-500 bold">
                {errorMessage}
              </span>
            )}
          </div>
        </form>
      ) : (
          <p className={`${pColor} text-xl md:text-lg xl:text-2xl text-center md:text-left mt-4 mb-8 lg:my-10`}>{result}</p>
        )}
    </>
  )
}

export default injectIntl(NewsletterForm)
