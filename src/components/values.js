import React from "react"
import { injectIntl, defineMessages, FormattedMessage } from "gatsby-plugin-intl"
import HandIcon from "./handIcon"

const messages = [
  defineMessages(
    {
      heading: {
        id: "values.0.heading",
        defaultMessage: "Nous pensons à plusieurs."
      },
      rationale: {
        id: "values.0.rationale",
        defaultMessage: "Mazurka favorisera l'échange : savoir qui répond, rédiger ensemble, valider collectivement, se donner des deadlines."
      }
    }),
  defineMessages(
    {
      heading: {
        id: "values.1.heading",
        defaultMessage: "Nous sommes dans plusieurs groupes."
      },
      rationale: {
        id: "values.1.rationale",
        defaultMessage: "Mazurka permettra de se connecter à plusieurs groupes : que ce soit son AMAP ou son club de roller derby."
      }
    }),
  defineMessages(
    {
      heading: {
        id: "values.2.heading",
        defaultMessage: "Nous avons des groupes différents."
      },
      rationale: {
        id: "values.2.rationale",
        defaultMessage: "Mazurka s'adaptera aux pratiques de chaque collectif\u00A0: petits ou grands, peu connectés ou très connectés."
      }
    }
  ),
  defineMessages(
    {
      heading: {
        id: "values.3.heading",
        defaultMessage: "Nous faisons des erreurs"
      },
      rationale: {
        id: "values.3.rationale",
        defaultMessage: "Mazurka fera de son mieux pour prendre en compte nos maladresses, hésitations et revirements."
      }
    }
  ),
  defineMessages(
    {
      heading: {
        id: "values.4.heading",
        defaultMessage: "Notre temps est précieux."
      },
      rationale: {
        id: "values.4.rationale",
        defaultMessage: "Mazurka sera efficace et rapide, pour gérer des centaines de messages, mais surtout pour mieux travailler ensemble."
      }
    }
  ),
  defineMessages(
    {
      heading: {
        id: "values.5.heading",
        defaultMessage: "Notre attention est limitée."
      },
      rationale: {
        id: "values.5.rationale",
        defaultMessage: "Mazurka veillera à ne pas créer de sentiment d’urgence. Les rappels et notifications seront là pour nous aider à répondre, pas pour nous rendre accros."
      }
    }
  ),
  defineMessages(
    {
      heading: {
        id: "values.6.heading",
        defaultMessage: "Nous ne voulons pas être pisté·es."
      },
      rationale: {
        id: "values.6.rationale",
        defaultMessage: "Mazurka déjouera les <italic>trackers</italic> présents dans de nombreux emails et ne sera jamais financé par de la publicité."
      }
    }
  ),
  defineMessages(
    {
      heading: {
        id: "values.7.heading",
        defaultMessage: "Nous n'aimons pas les grandes oreilles."
      },
      rationale: {
        id: "values.7.rationale",
        defaultMessage: "Mazurka s'assurera que nos communications ne soient lisibles que par les personnes qui en ont besoin."
      }
    }),
  defineMessages(
    {
      heading: {
        id: "values.8.heading",
        defaultMessage: "Nous n'aimons pas être dépendant·es.",
      },
      rationale: {
        id: "values.8.rationale",
        defaultMessage: "Mazurka sera un logiciel libre. Il pourra être installé sur d'autres serveurs, importer et exporter nos données et être modifié selon nos besoins.",
      }
    }
  )

]

const Values = () => {
  return (
    <>
      {messages.map((v, i) => (
        <div className={i % 2 != 1 ? "my-12" : "my-12 flex flex-col items-end"}>
          <div className="values-title">
            {i % 2 != 1 && <HandIcon left={false} />}
            <h3 className="h3-values">
              <FormattedMessage {...v.heading} />
            </h3>
            {i % 2 != 0 && <HandIcon left />}
          </div>
          <p className="p-values">
            <FormattedMessage {...v.rationale}
              values={{
                italic: (...chunks) => <span className="italic">{chunks}</span>,
              }} />
          </p>
        </div>
      ))}
    </>
  )
}

export default injectIntl(Values)
