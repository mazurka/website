import React from "react"
import { useStaticQuery, graphql } from "gatsby"

const FeaturesPoint = ({ children }) => {
  const {star} = useStaticQuery(graphql`
  query {
    star: file(relativePath: { eq: "star.svg" }) {
      publicURL
    }
  } 
`
)
  return (
    <li className="flex">
      <img className="li-bullet" src={star.publicURL} alt="star" />
      <p className="p-features">{children}</p>
    </li>
  )
}

export default FeaturesPoint
