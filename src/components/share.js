import React from "react"
import { FaFacebookF, FaTwitter } from "react-icons/fa"
import {
  FacebookShareButton,
  TwitterShareButton,
} from "react-share";

export default function Share({ url, title, excerpt }) {


  return (
    <div>
      <FacebookShareButton url={url} quote={`${title}: ${excerpt}`} hashtag="Mazurka" >
        <FaFacebookF color="#1a202c" size="1.5em" className="mr-4 focus:outline-none"/>
      </FacebookShareButton >
      <TwitterShareButton url={url} title={title} hastag={["Mazurka", "Mail"]} >
        <FaTwitter color="#1a202c" size="1.5em" />
       </TwitterShareButton> 

    </div>
  )
}
