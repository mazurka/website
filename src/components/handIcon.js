import React from 'react'
import Image from "gatsby-image"
import { useStaticQuery, graphql } from "gatsby"

const HandIcon = ({left}) => {
  const {handRight, handLeft} = useStaticQuery(graphql`
    query {
      handRight: file(relativePath: { eq: "hand-purple.png" }) {
        childImageSharp {
          fixed(width: 50) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      handLeft: file(relativePath: { eq: "hand-purple-left.png" }) {
        childImageSharp {
          fixed(width: 50) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `
  )
  return (
    <Image
      fixed={left ? handLeft.childImageSharp.fixed : handRight.childImageSharp.fixed}
      className={left ? "h-12 w-12 ml-4" : "h-12 mr-4"}
      style={{minWidth: "50px"}}
      alt="hand"
    />
      
  )
}

export default HandIcon