/**
 * Bio component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import Image from "gatsby-image"


const Bio = ({author, avatar}) => {

  return (
    <div
      className=" flex mb-12 items-center mb-8"
    >
      {/* <p>"${JSON.stringify(avatar)}"</p> */}
      <Image
        fluid={avatar.childImageSharp.fluid}
        alt={author.name}
        className="mr-4 mb-0 w-16 h-auto rounded-full"
        style={{}}

      />
      <p> {author.id} </p>
      <p>{author.bio}</p>
    </div>
  )
}

export default Bio
