import React from "react"
import { useStaticQuery, graphql } from "gatsby"

const PricingPoint = ({ children }) => {
  const {check} = useStaticQuery(graphql`
  query {
    check: file(relativePath: { eq: "checkmark.svg" }) {
      publicURL
    }
  } 
`
)
  return (
    
    <li className="flex">
      <img className="li-bullet" src={check.publicURL} alt="✓" />
      <p className="p-card">
          {children}
      </p>
    </li>
  )
}

export default PricingPoint
