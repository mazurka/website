import React from 'react'
import { injectIntl, FormattedMessage } from "gatsby-plugin-intl"

const FooterModal = () => {
  return (
    <div class="flex flex-wrap justify-center lg:justify-start">
      <input className="hidden" type="checkbox" id="show-mentions-legales" />
      <label htmlFor="show-mentions-legales" className="font-base text-white cursor-pointer underline"><span>
        <FormattedMessage id="footer.link"
          defaultMessage="mentions légales et protections des données personnelles" />
      </span></label>
      <div className="hidden text-left" id="mentions-legales">
        <p className="text-3xl"><strong>
          <FormattedMessage id="legal.title"
            defaultMessage="Mentions légales" />
        </strong></p>
        <p className="text-xl"><strong>
          <FormattedMessage id="legal.editors.title"
            defaultMessage="Éditeur·ices" />
        </strong></p>
        <p className="whitespace-pre-line"><FormattedMessage id="legal.editors.content"
          defaultMessage={`Mazurka\n<code>contact@mazurka.app</code>`}
          values={{
            code: (...chunks) => <code>{chunks}</code>
          }}
        />
        </p>
        <p className="text-xl"><strong>
          <FormattedMessage id="legal.hosting.title"
            defaultMessage="Hébergement du site web" />
        </strong></p>
        <p className="whitespace-pre-line"><FormattedMessage
          id="legal.hosting.content"
          defaultMessage={`Association Framasoft
c/o Locaux Motiv
10 bis, rue Jangot
69007 Lyon
France
<code>https://contact.framasoft.org/</code>`}
          values={{
            code: (...chunks) => <code>{chunks}</code>
          }} /></p>
        <p className="text-xl"><strong>
          <FormattedMessage id="legal.data-policy.title"
            defaultMessage="Traitement des données à caractères personnelles" />
        </strong></p>
        <FormattedMessage
          id="legal.data-policy.content"
          defaultMessage={`
        <p>Les données collectées ne seront transmises à des tiers que pour le strict nécessaire des aspects
          techniques de leur traitement.</p>
        <p><strong>Formulaires de contact</strong>\u{202F}: les informations recueillies sur les formulaires de contact
          sont enregistrées dans un fichier informatisé par l'association potager.org pour vous envoyer des
          nouvelles de temps en temps. La base légale du traitement est le consentement au traitement de ces données
          pour cette unique finalité.</p>
        <p><strong>Formulaire d'enquête</strong>\u{202F}: les informations recueillies sur le formulaire d'enquête sont
          enregistrées dans un fichier informatisé par l'entreprise TYPEFORM SL pour nous permettre de mieux
          connaître vos attentes et si vous le souhaitez vous donnez des nouvelles ou prendre contact avec vous. La
          base légale du traitement est le consentement au traitement de ces données pour ces finalités.</p>
        <p>Les données collectées seront uniquement communiquées à l'équipe de Mazurka.</p>
        <p>Les adresses emails saisies dans ces deux formulaires seront conservées tant que durera le projet Mazurka, tant que nous aurons donc des nouvelles à
          vous communiquer, et tant que vous souhaitez en recevoir.</p>
        <p>Les autres données collectées ne sont pas à caractère personnelle et seront uniquement traitées sous une forme agrégée. Les réponses individuelles seront effacées dans un délai d'un mois suivant la fin de l'enquête.</p>
        <p>Vous pouvez accéder aux données vous concernant, les rectifier, demander leur effacement ou exercer votre
          droit à la limitation du traitement de vos données.</p>
        <p>Pour exercer ces droits ou pour toute question sur le traitement de vos données dans ce dispositif, vous
          pouvez contacter l'équipe de Mazurka à l'adresse email <code>contact@mazurka.app</code>.</p>
        <p>Consultez le site cnil.fr pour plus d’informations sur vos droits.</p>
        <p>Si vous estimez, après nous avoir contactés, que vos droits «\u{202F}Informatique et Libertés\u{202F}» ne sont pas
          respectés, vous pouvez adresser une réclamation à la CNIL.</p>
        `}
          values={{
            p: (...chunks) => <p>{chunks}</p>,
            em: (...chunks) => <em>{chunks}</em>,
            strong: (...chunks) => <strong>{chunks}</strong>,
            code: (...chunks) => <code>{chunks}</code>
          }} />
        <p className="text-xl"><strong>
          <FormattedMessage id="legal.stats.title"
            defaultMessage="Statistiques et cookies" />
        </strong></p>
        <FormattedMessage
          id="legal.stats.content"
          defaultMessage={`
        <p>Ce site utilise l’outil Matomo, un logiciel libre d’analyse de fréquentations de site Internet, disponible à l'adresse https://matomo.org/ Matomo utilise des <em>cookies</em> stockant des identifiants sur votre ordinateur. Ceux-ci nous permettent d’analyser l’utilisation de notre site web. Les informations concernant votre utilisation du site (y compris l'adresse IP de connexion) sont transmises à notre installation de Matomo hébergée par l’association potager.org. Au cours de ce processus, l'adresse IP est tronquée de façon anonyme et nous ne connaissons donc pas votre identité. Ni ce cookie ni les données générées par Matomo ne sont pas transmises à des tiers.</p>
        `}
          values={{
            p: (...chunks) => <p>{chunks}</p>,
            em: (...chunks) => <em>{chunks}</em>,
            strong: (...chunks) => <strong>{chunks}</strong>,
            code: (...chunks) => <code>{chunks}</code>
          }}
        />

      </div>
    </div>
  )
}

export default injectIntl(FooterModal)